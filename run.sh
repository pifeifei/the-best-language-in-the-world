#!/bin/bash


ask=世界上最好的语言是
bastLanguage='{LANG} 是世界上最好的语言'

# set /p input="世界上最好的语言是:"
read -p "${ask}:" input
# echo 世界上最好的语言是: go
# input=PHP

# OS: linux/macos/windwos
# ver: 版本
bastLanguage=${bastLanguage//\{LANG\}/$input}
echo $bastLanguage

# shell language
sLang=${SHELL##*/}
ver=$(bash --version | grep -oP '\d+\.\d+\.\d+')
echo "${sLang}(${ver}): ${input} is the best language in the world."

# has=$(which g++ 2>/dev/null)
# [ ! -z "$has" ] && g++ -x c++ - << EOF && ./a.out "${input}"
# #include <iostream>
# int main() {
#   std::cout << "C++: " << "${input}" << " is the best language in the world." << std::endl;
#   return 0;
# }
# EOF

# Golang language
has=$(which go 2>/dev/null)
if [ ! -z "$has" ] ;
then
    ver=$(go version | grep -oP '\d+\.\d+\.\d+')
    # node -e "console.log('NodeJS/JavaScript(${ver}): ${input} is the best language in the world.');"
    message="GO(${ver}): ${input} is the best language in the world."
    cat > tmp/hello.go <<EOF
package main

import "fmt"

func main() {
    fmt.Println("${message}")
}
EOF
    go run tmp/hello.go
fi


# Java language
has=$(which java 2>/dev/null)
[ ! -z "$has" ] && ver=$(java -version 2>&1 >/dev/null | head -1 | grep -oP '\d+\.\d+\.\d+_\d+')
[ ! -z "$has" ] && java -cp code hello "Java(${ver}): ${input} is the best language in the world."
# javac code/hello.java # 编译
# 编译、执行分开执行的，就保留了源码


# javascript(nodejs) language
has=$(which node 2>/dev/null)
[ ! -z "$has" ] && ver=$(node -v  | grep -oP '\d+\.\d+\.\d+')
[ ! -z "$has" ] && node -e "console.log('NodeJS/JavaScript(${ver}): ${input} is the best language in the world.');"


# PHP language
has=`which php 2>/dev/null`
[ ! -z "$has" ] && ver=$(php -v | head -1 | grep -oP '\d+\.\d+\.\d+')
[ ! -z $has ] && php -r "echo 'PHP(${ver}): ${input} is the best language in the world.';"
echo


# Ruby language
has=`which ruby 2>/dev/null`
[ ! -z "$has" ] && ver=$(ruby -v | head -1 | grep -oP '\d+\.\d+\.\d+')
[ ! -z $has ] && ruby -e "print 'Ruby(${ver}): ${input} is the best language in the world.'"
echo


# Python language
has=`which Python 2>/dev/null`
[ ! -z "$has" ] && ver=$(Python -V | head -1 | grep -oP '\d+\.\d+\.\d+')
[ ! -z $has ] && Python -c "print('Python(${ver}): ${input} is the best language in the world.')"
echo
